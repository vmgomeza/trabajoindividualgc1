#Codigo Diametro Mínimo y Máximo - Trabajo Individual (1-Febrero-2021) Valeria Gomez
#Dado una serie de puntos,calcular el diametro minimo y maximo
from matplotlib import pyplot as plt
from random import randint
from math import *

#Retorna una lista de puntos (coordenadas) creados aleatoriamente (0 a 50)
def PuntosAlea(cant):
    return [[randint(0,16),randint(0,16)] for _ in range(cant)]

# Retorna la distancia euclidiana entre dos puntos
def distancia(a, b):
    return sqrt(pow(a[0] - b[0], 2) + pow(a[1] - b[1], 2))

# Calcula la distancia minima y maxima
def Calculardistancia(puntos):

    global p1,p2,p3,p4

    n = len(puntos)
    print(n)
    #Inicializa los puntos
    dminima=100000
    dmaxima=0

    for i in range(n+1):
        for j in range(i, n):
            aux =distancia(puntos[i], puntos[j])

            if (aux > dmaxima):
                dmaxima = aux
                p1=puntos[i]
                p2=puntos[j]

            if (aux < dminima and aux>0):
                dminima = aux
                p3 = puntos[i]
                p4 = puntos[j]

    return dmaxima,dminima

#Grafica los puntos y las distancia entre los puntos
def grafico(coords):
    x,y = zip(*coords) #Divide en dos listas (x,y)
    plt.scatter(x,y) #Grafica los puntos

    x_values = [p1[0], p2[0]]
    y_values = [p1[1], p2[1]]
    plt.plot(x_values, y_values)

    x_values = [p3[0], p4[0]]
    y_values = [p3[1], p4[1]]
    plt.plot(x_values, y_values)

    plt.show()


puntos = PuntosAlea(15)
dis = Calculardistancia(puntos)
print("Puntos:", puntos)
print ("Distancia maxima es:",dis[0])
print ("Distancia minima es:",dis[1])
grafico(puntos)





