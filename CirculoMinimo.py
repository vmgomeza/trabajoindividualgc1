#Codigo Circulo Minimo - Trabajo Individual (1-Febrero-2021) Valeria Gomez
#Dado una serie de puntos,calcular el circulo minimo que contenga a todos los puntos

from math import sqrt
from matplotlib import pyplot as plt
from random import randint

#Retorna una lista de puntos (coordenadas) creados aleatoriamente
def PuntosAlea(cant):
    return [[randint(0,10),randint(0,10)] for _ in range(cant)]

# Define el limite
INF = 10 ** 18

# Retorna la distancia euclidiana entre dos puntos
def distancia(a, b):
    return sqrt(pow(a[0] - b[0], 2) + pow(a[1] - b[1], 2))

#Ve si un punto esta dentro o en los limites de un circulo
def dentrocirculo(c, p):
    return distancia(c[0], p) <= c[1]

# Define el centro de un circulo con tres puntos
def centrocirculo(bx, by, cx, cy):
    B = bx * bx + by * by
    C = cx * cx + cy * cy
    D = bx * cy - by * cx
    if D==0:
        D=0.000001
    return [(cy * B - by * C) // (2 * D),
            (bx * C - cx * B) // (2 * D) ]

# Define un circulo con tres puntos
def circulo3(A, B, C):
    I = centrocirculo(B[0] - A[0], B[1] - A[1],
                                C[0] - A[0], C[1] - A[1])

    I[0] += A[0]
    I[1] += A[1]
    return [I, distancia(I, A)]

# Retorna un circulo con 2 puntos
def circulo2(A, B):
    C = [(A[0] + B[0]) / 2, (A[1] + B[1]) / 2]
    return [C, distancia(A, B) / 2]

# Verifica si el circulo contiene todo los puntos
def circulovalido(c, P):
    for p in P:
        if (dentrocirculo(c, p) == False):
            return False
    return True

#Funcion para encontrar el circulo minimo dado varios puntos
def circulominimo(P):
    n = len(P)

    if (n == 0):
        return [[0, 0], 0]
    if (n == 1):
        return [P[0], 0]

    #Inicializa el circulo
    cmin = [[0,0], INF]

    for i in range(n+1):
        for j in range(i + 1, n):

            aux = circulo2(P[i], P[j])
            if (aux[1] < cmin[1] and circulovalido(aux, P)):
                cmin = aux

    for i in range(n):
        for j in range(i + 1, n):
            for k in range(j + 1, n):

                aux = circulo3(P[i], P[j], P[k])
                if (aux[1] < cmin[1] and circulovalido(aux, P)):
                    cmin = aux

    return cmin

# Función que grafica los puntos y el circulo mínimo
def grafico(puntos):

    x,y = zip(*puntos) #Divide en dos listas (x,y)
    plt.scatter(x,y) #Grafica los puntos

    draw_circle = plt.Circle((cmin[0][0],cmin[0][1]),round(cmin[1], 6), fill=False)
    plt.gcf().gca().add_artist(draw_circle)
    plt.show()

puntos = PuntosAlea(20)
cmin = circulominimo(puntos)
print("Puntos:", puntos)
print("Centro = ( ", int(cmin[0][0]), ",", cmin[0][1],
      ") Radio = ", round(cmin[1], 6))
grafico(puntos)


