#Codigo Signalidad Triangulo(Area Asignada) - Trabajo Individual (4-Enero-2021) Valeria Gomez
#Dado los vertices del triangulo calcula a que lado se ubica un punto respecto a una recta

from matplotlib import pyplot as plt
from random import randint
import numpy as np

#Creamos 3 puntos aleatoriamente
def Vertices():
    global Ax,Ay,Bx,By,Cx,Cy
    Ax,Ay = randint(-10,10),randint(-10,10)
    Bx,By = randint(-10,10),randint(-10,10)
    Cx,Cy = randint(-10,10),randint(-10,10)
    return Ax,Ay,Bx,By,Cx,Cy

#Grafica los puntos 
def grafico():
    plt.plot([Ax,Bx,Cx,Ax],[Ay,By,Cy,Ay])
    plt.show()

#Creamos las matrices con los puntos aleatorios (Varia el orden de la matriz dependiendo del punto a calcular)
def Matriz():
    global Mabc,Macb,Mbca
    #Matriz para calcular C
    Mabc = [[1, 1, 1], [Ax, Bx, Cx], [Ay, By, Cy]]
    # Matriz para calcular b
    Macb = [[1, 1, 1], [Ax, Cx, Bx], [Ay, Cy, By]]
    #Matriz para calcular A
    Mbca = [[1, 1, 1], [Bx, Cx, Ax], [By, Cy, Ay]]
    return Mabc,Macb,Mbca

#Calcula el determinante de la matriz
def Determinante(Matriz):
    return np.around(0.5 * np.linalg.det(Matriz))

#Dependiendo del determinante indica en que direccion esta el punto
def Lado(det):
    if det > 0:
        print("esta a la izquierda,tenemos un giro antihorario")
    elif det < 0:
        print("esta a la derecha,tenemos un giro horario")
    else:
        print("se encuentra sobre la recta")

Vertices()
print("Los vertices del punto A son:",Ax,Ay)
print("Los vertices del punto B son:",Bx,By)
print("Los vertices del punto C son:",Cx,Cy)

Matriz()
detA = Determinante(Mbca)
detB = Determinante(Macb)
detC = Determinante(Mabc)

print("El determinante para A es:",detA)
print("El determinante para B es:",detB)
print("El determinante para C es:",detC)

print("El punto A respecto a B y C")
print(Lado(detA)," ")
print("El punto B respecto a A y C")
print(" ",Lado(detB))
print("El punto C respecto a A y B")
print(" ",Lado(detC))
grafico()

